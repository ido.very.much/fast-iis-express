# Fast IIS Express

Help you run .net website fast. (mostly use at the building prosses).

### Build
Build the project with visual studio

### Who to use:
* Open "Fast IIS Express"
* Open the "root" folder (you have button when you open the app).
* then in the IISE folder copy the IIS Express files you want to use (your preferred version)
* Insert your website folder location
* Enter the start button

That it. Enjoy!

![Screenshot](./Screenshot.png "Screenshot")