﻿using System;
using System.Windows;
using System.Json;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Controls;
using System.Management;

namespace RunAsp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string path, runStartup;

        public JsonValue Settings;

        public Process myProcess;

        public Uri openurl;

        public object ProxsyServer;

        string startup;


        public MainWindow()
        {
            InitializeComponent();
            HideScriptErrors(Browser, true);
            this.startup = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\ASP_Server.lnk";

            this.path = Directory.GetCurrentDirectory() + "\\System\\";

            string config = this.path + "Config.json";

            if (File.Exists(config))
            {
                this.Settings = JsonObject.Parse(File.ReadAllText(config));
                this.Port.Text = this.Settings["Port"];
                this.Dictionary.Text = this.Settings["Dictionary"];
                this.OnAppStart.IsChecked = this.Settings["OnAppStart"];
                this.MachinePort.Text = this.Settings["MachinePort"];

                if ((bool)this.Settings["OnAppStart"] || (bool)this.Settings["Once"])
                {
                    SaveSetting("Once", false);
                    startIISExpress();
                }
                this.OnWindowStart.IsChecked = this.Settings["OnWindowStart"];
            }
            else
            {
                this.Settings = new JsonObject();
                this.Settings["Port"] = this.Port.Text;
                this.Settings["MachinePort"] = this.MachinePort.Text;
                this.Settings["Dictionary"] = this.Dictionary.Text;
                this.Settings["OnAppStart"] = this.OnAppStart.IsChecked;
                this.Settings["OnAppStart"] = this.OnAppStart.IsChecked;
                this.Settings["OnWindowStart"] = this.OnWindowStart.IsChecked;
                this.Settings["Once"] = false;
            }
        }

        private static void KillProcessAndChildrens(int pid)
        {
            ManagementObjectSearcher processSearcher = new ManagementObjectSearcher
              ("Select * From Win32_Process Where ParentProcessID=" + pid);
            ManagementObjectCollection processCollection = processSearcher.Get();

            try
            {
                Process proc = Process.GetProcessById(pid);
                if (!proc.HasExited) proc.Kill();
            }
            catch (ArgumentException)
            { }

            if (processCollection != null)
            {
                foreach (ManagementObject mo in processCollection)
                {
                    KillProcessAndChildrens(Convert.ToInt32(mo["ProcessID"])); //kill child processes(also kills childrens of childrens etc.)
                }

            }
        }
                public void HideScriptErrors(WebBrowser wb, bool Hide)
        {
            FieldInfo fiComWebBrowser = typeof(WebBrowser)
                .GetField("_axIWebBrowser2",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return;
            object objComWebBrowser = fiComWebBrowser.GetValue(wb);
            if (objComWebBrowser == null) return;
            objComWebBrowser.GetType().InvokeMember(
                "Silent", BindingFlags.SetProperty, null, objComWebBrowser,
                new object[] { Hide });
        }

        public void AdminIISE(string usePort, string proxyport)
        {
            SaveSetting("Once", true);
            ProcessStartInfo StartInfo = new ProcessStartInfo
            {
                FileName = this.path + "..\\RunAsp.exe",
                UseShellExecute = true,
                Verb = "runas",
                CreateNoWindow = true
            };

            try
            {
                this.Hide();
                Process p = Process.Start(StartInfo);
                p.WaitForExit();
                p.Close();
                Environment.Exit(0);
            }
            catch (Exception err)
            {
                this.Show();
                MessageBoxResult r = MessageBox.Show("Can't start proxy server.\nSystem Messgae -> \"" + err.Message + ")\"\nIf you don't want to use proxy server clear the proxy ip field. Do you want to try again?\n", "Proxy Error", MessageBoxButton.YesNo);
                if (r == MessageBoxResult.Yes)
                {
                    AdminIISE(usePort, proxyport);
                }
            }
        }
        public void startIISExpress()
        {
            string folder = this.path + "IISE";

            string editconfig = File.ReadAllText(this.path + "Host.config");

            string protocol;

            int[] getByLog(string start, string find)
            {
                int findStart = editconfig.IndexOf(start);

                findStart += editconfig.Substring(findStart).IndexOf(find) + find.Length + 2;

                int findEnd = findStart + editconfig.Substring(findStart).IndexOf('"');

                int[] a = { findStart, findEnd };
                return a;
            }

            int[] findPro = getByLog("<binding", "protocol");

            protocol = editconfig.Substring(findPro[0]).Substring(0, findPro[1] - findPro[0]);

            void toFix(string start, string find, string replace)
            {

                int[] findLoc = getByLog(start, find);

                editconfig = editconfig.Substring(0, findLoc[0]) + replace + editconfig.Substring(findLoc[1]);
            }
            string checkLoc = (string)this.Settings["Dictionary"];

            if (checkLoc.Length > 0 && (checkLoc[0].Equals('/') || checkLoc[0].Equals('\\')))
            {
                checkLoc = this.path + checkLoc.Substring(1);
            }

            checkLoc = checkLoc.Replace("/", "\\");

            string usePort = (string)this.Settings["MachinePort"];

            toFix("<binding", "bindingInformation", "*:" + usePort + ":localhost");
            toFix("<virtualDirectory", "physicalPath", checkLoc);

            File.WriteAllText(this.path + "Host.config", editconfig);

            string proxyport = (string)this.Settings["Port"];

            int t;

            bool goNext = true;
            if (!proxyport.Equals("") && Int32.TryParse(proxyport, out t))
            {
                try
                {
                    ProxsyServer = new iis();
                    ((iis)ProxsyServer).StartProxsy(usePort, proxyport);
                }
                catch (Exception err)
                {
                    AdminIISE(usePort, proxyport);
                    goNext = false;
                }
            }

            if (goNext)
            {
                ProcessStartInfo StartInfo = new ProcessStartInfo
                {
                    FileName = this.path + "IISE\\iisexpress.exe",
                    Arguments = "/config:" + this.path + "Host.config",
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardInput = true
                };

                this.myProcess = Process.Start(StartInfo);

                this.UseButton.Content = "Stop";

                openurl = new Uri(protocol + "://localhost:" + usePort);

                this.Browser.Source = openurl;

                this.UrlOpen.NavigateUri = openurl;
            }
            else
            {
                if (this.myProcess != null)
                    KillProcessAndChildrens(this.myProcess.Id);
            }

        }

        public void OpenUrl(object sender, RoutedEventArgs e)
        {
            ProcessStartInfo StartInfo = new ProcessStartInfo
            {
                FileName = "cmd",
                Arguments = "/C start " + openurl.ToString(),
                UseShellExecute = false,
                CreateNoWindow = true
            };

            Process.Start(StartInfo);
        }

        public void SaveSetting(string name, object value)
        {
            this.Settings[name] = value.ToString();

            File.WriteAllText(this.path + "Config.json", this.Settings.ToString());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.UseButton.Content.Equals("Stop"))
            {
                if (this.myProcess != null)
                    KillProcessAndChildrens(this.myProcess.Id);
                { }
                try
                {
                    ((iis)ProxsyServer).StopProxsy();
                }
                catch (Exception err)
                { }
                this.UseButton.Content = "Start";
            }
            else
            {
                startIISExpress();
            }
        }

        private void FocusOut_Dic(object sender, RoutedEventArgs e)
        {
            SaveSetting("Dictionary", this.Dictionary.Text);
        }

        private void FocusOut_Port(object sender, RoutedEventArgs e)
        {
            SaveSetting("Port", this.Port.Text);
        }

        private void FocusOut_MP(object sender, RoutedEventArgs e)
        {
            SaveSetting("MachinePort", this.MachinePort.Text);
        }

        private void AppStart(object sender, RoutedEventArgs e)
        {
            SaveSetting("OnAppStart", this.OnAppStart.IsChecked);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(this.myProcess != null)
                KillProcessAndChildrens(this.myProcess.Id);
            try
            {
                if (ProxsyServer != null)
                    ((iis)ProxsyServer).StopProxsy();
            }
            catch (Exception err)
            { }
        }

        private void WindowsStart(object sender, RoutedEventArgs e)
        {

            if ((bool)this.OnWindowStart.IsChecked)
            {
                IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
                IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(startup);
                shortcut.Description = "Shortcut for ASP Fast Server";
                shortcut.TargetPath = this.path + "RunApp.exe";
                shortcut.Save();
            }
            else
            {
                if (File.Exists(this.startup))
                {
                    File.Delete(this.startup);
                }
            }
            SaveSetting("OnAppStart", this.OnWindowStart.IsChecked);
        }

        private void openFolder(object sender, RoutedEventArgs e)
        {
            ProcessStartInfo StartInfo = new ProcessStartInfo
            {
                FileName = "cmd",
                Arguments = "/C start " + this.path,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            Process.Start(StartInfo);
        }
    }
}
